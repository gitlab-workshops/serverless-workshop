# Lab 6: Deploying a function to Lambda

GitLab CI is very generic and can run any action. Therefore we can also use it do deploy an AWS Lambda using the [Serverless Framework](https://serverless.com/).

Ideally Lambda would also offer a Knative compatible endpoint to deploy services, but this is wishful thinking.

In this lab, you will deploy a sample Python function to AWS Lambda cloud.

**Pre-requisites**: You need to have an AWS account and know how to get your API keys.

## Configure your AWS API Keys

Head over to the Settings CI/CD view and add two environment variables for your AWS keys:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`

![awskeys](images/keys.png)

With those keys setup you can now deploy to Lambda by running `serverless deploy` via your GitLab CI as shown in the next section.

## `serverless.yaml` Configuration

Using the WebIDE copy the `serverless.yaml` file in this `lab6/` directory to the root of the repository.

```
service: aws-python-simple-http-endpoint

frameworkVersion: ">=1.2.0 <2.0.0"

provider:
  name: aws
  runtime: python2.7 # or python3.6, supported as of April 2017

functions:
  currentTime:
    handler: lab6/handler.endpoint
    events:
      - http:
          path: ping
          method: get
```

This file is typical of the Serverless framework and the Knative `tm` CLI uses a similar syntax.

## GitLab CI Configuration

In the lab6 directory view the `.gitlab-ci.yml` file:

```yaml
stages:
  - deploy-lambda

deploy-hello-function:
  stage: deploy-lambda
  environment: test
  image: gcr.io/triggermesh/serverless
  script:
    # uncomment the next line to remove the function 
    #- serverless remove
    # comment the next line to remove the function
    - serverless deploy
```

Using the Web IDE you can copy this CI configuration and move it to the **root** of this repository. Modifying the `.gitlab-ci.yml` file will automatically trigger a Pipeline execution and the Lambda should get deployed.

## Test the Function

Check the logs of the Pipeline job and find the URL of the function:

![lambdalog](images/lambda.png)

Call the function by opening your browser at the URL you found in the logs or use `curl`.

To remove your function run `serverless remove` via CI/CD by commenting out the `serverless deploy` step like so:

```
stages:
  - deploy-lambda

deploy-hello-function:
  stage: deploy-lambda
  environment: test
  image: gcr.io/triggermesh/serverless
  script:
    # uncomment the next line to remove the function 
    - serverless remove
    # comment the next line to remove the function
    #- serverless deploy
```

## Congratulations!!

You just deployed an AWS Lambda function via GitLab.

![Celebration Tanuki](images/celebrate-tanuki.png)
