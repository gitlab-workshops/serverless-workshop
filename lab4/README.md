# Lab 4: GitLab Eventing

Now that we have deployed a serverless function and a serverless application,
it is time to talk about eventing. Knative eventing is a system which makes
triggering serverless functions easier.

Instead of needing to send a payload to an endpoint, serverless functions can be
triggered by common GitLab workflow events, such as pushing a commit to a
project or opening a pull request.

We have taken care of some of the plumbing for you, so all you need to do is a few  more edits to your CI/CD configuration.

You will:

* Create a Secret via `kubectl`
* Create a GitLab event source via `kubectl`
* Deploy a function via the TriggerMesh console
* Check the GitLab events being sent to that function via the Pod logs on the TriggerMesh console.

## GitLab Secret

In your `lab4` directory, you will notice a file named `gitlabsecret.yaml`. This file defines the ACCESS_TOKEN to use the GitLab API and the SECRET_TOKEN to secure a GitLab webhook.
You do not need to edit them, as they will be edited for you in CI. The TriggerMesh Cloud needs this secret to be able to register a GitLab webhook in this very project.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlabsecret
type: Opaque
stringData:
  accessToken: REPLACE_WITH_TOKEN
  secretToken: REPLACE_WITH_SECRET
```

## The Event Source

You will also notice in the `lab4` directory of your
**workshop** project a file named `gitlabeventbinding.yaml` with the following content:

```yaml
apiVersion: sources.eventing.triggermesh.dev/v1alpha1
kind: GitLabSource
metadata:
  name: gitlabsample
spec:
  eventTypes:
    - push_events
    - merge_requests_events
    - issues_events
  projectUrl: https://gitlab.tanuki.host/REPLACE_WITH_USERNAME/workshop
  accessToken:
    secretKeyRef:
      name: gitlabsecret
      key: accessToken
  secretToken:
    secretKeyRef:
      name: gitlabsecret
      key: secretToken
  sink:
    apiVersion: serving.knative.dev/v1alpha1
    kind: Service
    name: gitlab-message-dumper
```

This file represents a Knative event source. It will automatically create a GitLab webhook (using the previously mentioned secret) in your project and send the events to a service called `gitlab-message-dumper`. We will explain what that is shortly.


## Update Your CI Configuration

To get eventing setup properly, you will need to edit the `.gitlab-ci.yaml` one more time in order to
deploy the Kubernetes API object we just mentioned.

Edit the top of the file to add a new `eventing` stage so that it looks like this:

```yaml
stages:
  - eventing
```

At the bottom of the file, create a new job in your new `eventing` stage by
appending the following contents:

```yaml
setup-eventing:
  stage: eventing
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - apt update -qq && apt install curl -yqq
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl
    - echo $TMCONFIG > tmconfig
    - sed -i -e "s/REPLACE_WITH_TOKEN/$CI_DEPLOY_PASSWORD/g" ./lab4/gitlabsecret.yaml
    - sed -i -e "s/REPLACE_WITH_SECRET/$SECRET_TOKEN/g" ./lab4/gitlabsecret.yaml
    - sed -i -e "s/REPLACE_WITH_USERNAME/$CI_DEPLOY_USER/g" ./lab4/gitlabeventbinding.yaml
  script:
    - kubectl --kubeconfig tmconfig -n $CI_DEPLOY_USER apply -f ./lab4/gitlabsecret.yaml
    - kubectl --kubeconfig tmconfig -n $CI_DEPLOY_USER apply -f ./lab4/gitlabeventbinding.yaml
```

This looks daunting, but we will break it down. This job will accomplish
the following:

* Install kubectl on the container.
* Dump the contents of your `$TMCONFIG` environment variable to a file.
* Dump the personal access token you created in lab2 to your `gitlabsecret.yaml`.
* Configure the correct project in `gitlabeventbinding.yaml`.
* Create a secret and event binding resource on your cluster.

## Creating the `message-dumper` Service.

At this time, GitLab is wired to Knative to be able to trigger a function when a GitLab event happens. But the function (aka Service) referenced does not exist.

Log-in to the TriggerMesh console and create the `message-dumper` service:

* Go to [https://cloud.triggermesh.io](https://cloud.triggermesh.io)
* Click on [Create Service](https://cloud.triggermesh.io/services)

It should look like the snapshot below:

![](./images/message-dumper.png)

## Create a GitLab event

Open the webIDE and make a dummy commit which will generate a push event

## Check the logs of your `message-dumper`

Head over to the TriggerMesh console, go to the Pods section and check the logs of your `message-dumper`.
